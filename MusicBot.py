import os
import logging
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackContext
from youtubesearchpython import VideosSearch
from pytube import YouTube
from pytube.exceptions import RegexMatchError
from pytube.exceptions import VideoUnavailable
from pytube.cli import on_progress


TOKEN = '6493998450:AAHKKXfyTpcgjuLSNdxuBixQh993vryk0zk'


SEARCH, SELECT_VIDEO = range(2)


search_results = []


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def start(update: Update, context: CallbackContext) -> int:
    update.message.reply_text("Hello! I'm a music bot. Please enter the title of the song you want to search.")
    return SEARCH


def search_music(update: Update, context: CallbackContext) -> int:
    search_query = update.message.text


    videos_search = VideosSearch(search_query, limit = 10)
    search_results = videos_search.result()

    update.message.reply_text("Here are the search results:")
    for i, result in enumerate(search_results['result']):
        update.message.reply_text(f"{i + 1}. {result['title']}")
    update.message.reply_text("Please select a video by entering its number.")
    context.user_data['search_results'] = search_results['result']
    return SELECT_VIDEO


def select_video(update: Update, context: CallbackContext) -> int:
    try:
        selected_video = int(update.message.text)
        search_results = context.user_data.get('search_results', [])

        if 1 <= selected_video <= len(search_results):
            video = search_results[selected_video - 1]
            video_url = f"https://www.youtube.com/watch?v={video['id']}"
            update.message.reply_text(f"You selected: {video['title']}. Enjoy the video: {video_url}")
        else:
            update.message.reply_text("Invalid selection. Please enter a valid video number.")
    except ValueError:
        update.message.reply_text("Invalid input. Please enter a valid video number.")
    return ConversationHandler.END

def download_audio(update: Update, context: CallbackContext) -> int:
    selected_video = int(update.message.text)
    search_results = context.user_data.get('search_results', [])

    if 1 <= selected_video <= len(search_results):
        video = search_results[selected_video - 1]
        video_url = f"https://www.youtube.com/watch?v={video['id']}"

        try:
            
            yt = YouTube(video_url, on_progress_callback=on_progress)
            audio_stream = yt.streams.filter(only_audio=True).first()

            
            audio_stream.download(output_path='downloads/')

            
            audio_file = f'downloads/{yt.title}.mp3'
            context.bot.send_audio(chat_id=update.effective_chat.id, audio=open(audio_file, 'rb'))
        except VideoUnavailable:
            update.message.reply_text("Sorry, the selected video is unavailable.")
        except RegexMatchError:
            update.message.reply_text("Sorry, I couldn't find audio for this video.")
    else:
        update.message.reply_text("Invalid selection. Please enter a valid video number.")

    return ConversationHandler.END
# Function to cancel the conversation
def cancel(update: Update, context: CallbackContext) -> int:
    update.message.reply_text("Music search canceled.")
    return ConversationHandler.END

def main():
    # updater = Updater(token=TOKEN, use_context=True)
    updater = Updater(token=TOKEN)

    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            SEARCH: [MessageHandler(Filters.text & ~Filters.command, search_music)],
            SELECT_VIDEO: [MessageHandler(Filters.text & ~Filters.command, select_video)],
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )

    
    dp.add_handler(conv_handler)
    # conv_handler.add_handler(CommandHandler('download', download_audio))

    
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
    
