# MusicTeleBot

![Avatar](https://github.com/BuiHyMIPT/PyProj/assets/92125606/aa198f78-eceb-430c-b17c-82099596b6a2)


<div align="center">
  <h1>Музыкальный бот Telegram из Ютубе</h1>
  Этот телеграмм-бот ищет музыку по вашему запросу и отправляет ее вам в чат. 

<br><br>

# Установки
Чтобы установить и начать работать локальную копию, выполните следующие действия:

<br>

### 📁 Клонировать этот репозиторий

   ```
   git clone https://github.com/BuiHyMIPT/PyProj.git
   ```
    
### 📦 Установить зависимости
   
   ```
   pip install -r requirements.txt
   ```

### 🔧 Настройка бота
<div align="left">
- Получите токен от <a href="https://t.me/BotFather">@BotFather</a>  
</div>

<div align="left">
- Insert your token:
</div>

   ```
   TOKEN = "INSERT_YOUR_TOKEN_HERE"
   ```
<div align="left">
- Ну там в коде я поставил мой токен в чат телеграма
</div>

### 🚀 Bot start

   ```
   python MusicBot.py
   ```